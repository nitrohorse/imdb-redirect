# IMDB to Mobile Redirect

## NOTICE
[IMDB.com](https://www.imdb.com) is now secured with HTTPS as of December 15, 2017! There is no need for this extension anymore. Thanks!

## Description
Browser extension that redirects all [insecure IMDB links](http://www.imdb.com/) to the [secure mobile site](https://m.imdb.com/). Based on the free and open-source [hooktube-redirect browser extension](https://github.com/metiis/hooktube-redirect).

## Install Locally
* Firefox: [temporary](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Temporary_Installation_in_Firefox)
* Chrome: [permanent](https://superuser.com/questions/247651/how-does-one-install-an-extension-for-chrome-browser-from-the-local-file-system/247654#247654)

## Develop Locally
* Clone the repo
* Install dependencies: 
	* `yarn` or `npm install`
* Run add-on in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext):
	* `yarn web-ext` or `npm run web-ext`
* Check dependencies for any known vulnerabilities:
	* `yarn vuln-check` or `npm run vuln-check`
* Bundle add-on into a zip file for distribution:
	* `yarn bundle` or `npm run bundle`
